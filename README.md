# Yale Accessibility Resources
Hosted site: https://yale-a11y.gitlab.io/yale-accessibility-resources/

## To Work on this Repository
* Clone the repository:
$ git clone https://gitlab.com/slynch-github/yale-accessibility-resources.git

* Change directory:
$ cd yale-accessibility-resources

* Install dependencies:
$ npm install

* Develop locally:
$ npm run build
